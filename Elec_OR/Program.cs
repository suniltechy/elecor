﻿using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;

namespace Elec_OR
{
    class Program
    {
        private static string filePath = string.Empty;
        private static string destinationFile = string.Empty;
        private static string connectionString = string.Empty;
        static void Main(string[] args)
        {
            GetConnection();
            ProcessFileData();
        }

        private static void GetConnection()
        {
            connectionString = ConfigurationManager.ConnectionStrings["panelCon"].ConnectionString;
        }

        private static void ProcessFileData()
        {
            var folderPath = ConfigurationManager.AppSettings["folderPath"];
            if (Directory.Exists(folderPath))
            {
                string[] fileEntries = Directory.GetFiles(folderPath);
                foreach (var file in fileEntries)
                {
                    Console.WriteLine("File Path : {0}", file);
                    Console.WriteLine("-------------------------");
                    if (File.Exists(file))
                    {
                        var fileName = Path.GetFileName(file);
                        Console.WriteLine("Processing File : {0}", fileName);
                        switch (fileName)
                        {
                            case "EasyPower.csv-Bus.csv": destinationFile = "bus2"; filePath = file; break;
                            case "EasyPower.csv-Busway.csv": destinationFile = "Busway2"; filePath = file; break;
                            case "EasyPower.csv-Current Transformer.csv": destinationFile = "CT2"; filePath = file; break;
                            case "EasyPower.csv-HV Breaker.csv": destinationFile = "HVBreaker2"; filePath = file; break;
                            case "EasyPower.csv-Load.csv": destinationFile = "Load2"; filePath = file; break;
                            case "EasyPower.csv-LV Breaker.csv": destinationFile = "LVBreaker2"; filePath = file; break;
                            case "EasyPower.csv-Motor.csv": destinationFile = "Motor2"; filePath = file; break;
                            case "EasyPower.csv-Panel Schedule.csv": destinationFile = "PanelSchedule2"; filePath = file; break;
                            case "EasyPower.csv-POC.csv": destinationFile = "POC2"; filePath = file; break;
                            case "EasyPower.csv-Switch.csv": destinationFile = "Switch2"; filePath = file; break;
                            case "EasyPower.csv-Utility.csv": destinationFile = "Utility2"; filePath = file; break;
                            case "Schedule.csv": destinationFile = "Schedule2"; filePath = file; break;
                            default: destinationFile = ""; filePath = ""; break;
                        }
                        if (!string.IsNullOrEmpty(filePath) && !string.IsNullOrEmpty(destinationFile) && !string.IsNullOrEmpty(connectionString))
                        {
                            DataTable dtCsv = GetDataTabletFromCSVFile(filePath);
                            CheckIfTableExists(dtCsv, destinationFile);
                            InsertDataIntoSQLServerUsingSQLBulkCopy(dtCsv);
                            ResetDefaults();
                            Console.WriteLine("{0} has been processed successfully", fileName);
                        }
                    }
                }

            }
            else
            {
                Console.WriteLine("Could'nt find the folder location, please check your app configuration file.");
            }
        }

        private static DataTable GetDataTabletFromCSVFile(string filePath)
        {
            DataTable csvData = new DataTable();
            try
            {
                var duplicateIndex = new List<int>();
                using (TextFieldParser csvReader = new TextFieldParser(filePath))
                {
                    csvReader.SetDelimiters(new string[] { "," });
                    csvReader.HasFieldsEnclosedInQuotes = true;
                    string[] colFields = csvReader.ReadFields();
                    foreach (string column in colFields)
                    {
                        var index = csvData.Columns.IndexOf(column);
                        if (index <= 0)
                        {
                            DataColumn datecolumn = new DataColumn(column);
                            datecolumn.AllowDBNull = true;
                            csvData.Columns.Add(datecolumn);
                        }
                        else
                        {
                            duplicateIndex.Add(index);
                        }
                    }
                    while (!csvReader.EndOfData)
                    {
                        string[] fieldData = csvReader.ReadFields();
                        List<string> uniqueData = new List<string>();
                        for (int i = 0; i < fieldData.Length; i++)
                        {
                            if (fieldData[i] == "")
                            {
                                fieldData[i] = null;
                            }
                            if (duplicateIndex != null && duplicateIndex.Count > 0)
                            {
                                if (duplicateIndex.Contains(i) == false)
                                {
                                    uniqueData.Add(fieldData[i]);
                                }
                            }
                            else
                            {
                                uniqueData.Add(fieldData[i]);
                            }
                        }
                        csvData.Rows.Add(uniqueData.ToArray());
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            return csvData;
        }

        private static void InsertDataIntoSQLServerUsingSQLBulkCopy(DataTable csvFileData)
        {
            try
            {
                using (SqlConnection dbConnection = new SqlConnection(connectionString))
                {
                    dbConnection.Open();
                    using (SqlBulkCopy s = new SqlBulkCopy(dbConnection))
                    {
                        s.DestinationTableName = destinationFile;
                        foreach (var column in csvFileData.Columns)
                            s.ColumnMappings.Add(column.ToString(), column.ToString());
                        s.WriteToServer(csvFileData);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private static void ResetDefaults()
        {
            filePath = string.Empty;
            destinationFile = string.Empty;
        }

        private static void CheckIfTableExists(DataTable dataTable, string destinationFile)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(connectionString))
                {
                    if (con.State == ConnectionState.Closed)
                        con.Open();
                    var checkTableIfExistsCommand = new SqlCommand("IF EXISTS (SELECT 1 FROM sysobjects WHERE name =  '" + destinationFile + "') SELECT 1 ELSE SELECT 0", con);
                    var exists = checkTableIfExistsCommand.ExecuteScalar().ToString().Equals("1");
                    if (!exists)
                    {
                        var createTableBuilder = new StringBuilder("CREATE TABLE [" + destinationFile + "]");
                        createTableBuilder.AppendLine("(");
                        foreach (DataColumn dc in dataTable.Columns)
                        {
                            createTableBuilder.AppendLine("  [" + dc.ColumnName + "] VARCHAR(MAX),");
                        }
                        createTableBuilder.Remove(createTableBuilder.Length - 1, 1);
                        createTableBuilder.AppendLine(")");
                        var createTableCommand = new SqlCommand(createTableBuilder.ToString(), con);
                        createTableCommand.ExecuteNonQuery();
                    }
                    else
                    {
                        var renamedTable = destinationFile + "-" + DateTime.Now.ToString("MM-dd-yy_HH:mm");
                        var renameQuery = "EXEC sp_rename '" + destinationFile + "', '" + renamedTable + "'";
                        var renameTableCommand = new SqlCommand(renameQuery, con);
                        renameTableCommand.ExecuteNonQuery();
                        CheckIfTableExists(dataTable, destinationFile);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
